# Number flowers (Zahlenblumen)

## Puzzle

This program solves the following math puzzle using brute-force:

> Given a set of five operands and a result, create an arithmetic expression using exactly four of the operands, so that, by inserting the right operators, you get the given result.
>
> Here's an easy example:
>
> Operands: { 2, 3, 4, 5, 6 }<br />
> Result: 120<br />
> Solution (one of many): 2 × 3 × 4 × 5 = 120

In its original format, the puzzle is in the shape of a flower where the five petals are the operands and the stamen (center) is the result – hence the name:

![Example](example.png){width=240px}

The original also does not specify what operators are allowed, whether operator precedence matters, or how non-integer divisions are to be handled.
This program uses the four basic operators (`+`, `-`, `*`, `/`), respects operator precedence, and discards expressions that contain non-integer (and, obviously, zero) division results.

## Code

The code does not try (hard) to be efficient.
The main goal was to experiment with C++(23) features like ranges, `std::generator`, deducing `this`, `std::expected` etc.
In fact, an earlier version with a traditional inheritance-based expression tree and visitor was actually about twice as fast as the current `std::variant`-based implementation.

The code uses `fmt` instead of `<format>`/`<print>` because, as of C++23, the latter does not work well with ranges.

## Building

Building the program requires at least GCC 14 and C++23 support.

To build the program and solve the above example:
```
meson setup builddir
ninja -C builddir

builddir/number-flowers --numbers 2 3 4 5 6 --result 120
```

Or build the program manually:
```
g++-14 -std=c++23 -I fmt-10.2.1/include -I cli11-2.4.1/include -DFMT_HEADER_ONLY \
    -o number-flowers number-flowers.cpp
```

Run the program with `--help` for more information.

The output for our example above would be:
```
(3 * (5 * (2 + 6))) = 120
(3 * (5 * (6 + 2))) = 120
(5 * (3 * (2 + 6))) = 120
(5 * (3 * (6 + 2))) = 120
(4 * (6 * (2 + 3))) = 120
(4 * (6 * (3 + 2))) = 120
(6 * (4 * (2 + 3))) = 120
(6 * (4 * (3 + 2))) = 120
(2 * (3 * (4 * 5))) = 120
(2 * (3 * (5 * 4))) = 120
(2 * (4 * (3 * 5))) = 120
(2 * (4 * (5 * 3))) = 120
(2 * (5 * (3 * 4))) = 120
(2 * (5 * (4 * 3))) = 120
(3 * (2 * (4 * 5))) = 120
(3 * (2 * (5 * 4))) = 120
(3 * (4 * (2 * 5))) = 120
(3 * (4 * (5 * 2))) = 120
(3 * (5 * (2 * 4))) = 120
(3 * (5 * (4 * 2))) = 120
(4 * (2 * (3 * 5))) = 120
(4 * (2 * (5 * 3))) = 120
(4 * (3 * (2 * 5))) = 120
(4 * (3 * (5 * 2))) = 120
(4 * (5 * (2 * 3))) = 120
(4 * (5 * (3 * 2))) = 120
(5 * (2 * (3 * 4))) = 120
(5 * (2 * (4 * 3))) = 120
(5 * (3 * (2 * 4))) = 120
(5 * (3 * (4 * 2))) = 120
(5 * (4 * (2 * 3))) = 120
(5 * (4 * (3 * 2))) = 120
```
