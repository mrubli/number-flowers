// This program solves the following math puzzle using brute-force:
//
//   Given a set of five operands and a result, create an arithmetic expression
//   using exactly four of the operands, so that, by inserting the right
//   operators, you get the given result.
//
// Here's an easy example:
//   Operands: { 2, 3, 4, 5, 6 }
//   Result: 120
//   Solution (one of many): 2 * 3 * 4 * 5 = 120
//
// In its original format, the puzzle is in the shape of a flower where the five petals are the
// operands and the stamen (center) is the result - hence the name. The original also does not
// specify what operators are allowed and whether operator precedence matters. This program uses the
// four basic operators and respects operator precedence.
//
// Check the README.md for build instructions and additional information.

#include <array>
#include <chrono>
#include <expected>
#include <generator>
#include <memory>
#include <print>
#include <ranges>
#include <tuple>
#include <variant>
#include <vector>

#include <CLI11.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wtautological-compare"
#include <fmt/format.h>
#include <fmt/ranges.h>
#pragma GCC diagnostic pop

#define ENABLE_ALLOCATION_TRACKING 0

constexpr auto ColorRed		= "\033[0;31m";
constexpr auto ColorGreen	= "\033[0;32m";
constexpr auto ColorNone	= "\033[0m";


//
// Allocaton tracking

#if ENABLE_ALLOCATION_TRACKING

struct AllocStats
{
	int		New{};
	int		ArrayNew{};
};

auto
format_as(const AllocStats& as)
{
	return fmt::format("{{ new: {}, new[]: {} }}",
		as.New, as.ArrayNew);
}

static auto AllocStat = AllocStats{};

void* operator new(std::size_t count)
{
	AllocStat.New++;

	if (count == 0)
		++count;	// avoid std::malloc(0) which may return nullptr on success

	if (void *ptr = std::malloc(count))
		return ptr;

	throw std::bad_alloc{};
}

// no inline, required by [replacement.functions]/3
void* operator new[](std::size_t count)
{
	AllocStat.ArrayNew++;

	if (count == 0)
		++count;	// avoid std::malloc(0) which may return nullptr on success

	if (void *ptr = std::malloc(count))
		return ptr;

	throw std::bad_alloc{};
}

void operator delete(void* ptr) noexcept
{
	std::free(ptr);
}

void operator delete(void* ptr, std::size_t) noexcept
{
	std::free(ptr);
}

void operator delete[](void* ptr) noexcept
{
	std::free(ptr);
}

void operator delete[](void* ptr, std::size_t) noexcept
{
	std::free(ptr);
}

#endif



namespace
{


//
// Generic helpers

// Creates a functor from a number of lambdas.
template<typename... Ts>
struct Functor : Ts...
{
	using Ts::operator()...;
};


template<typename C>
std::vector<std::vector<typename C::value_type>>
cartesian_self_product(const C& v, int n)
{
	if(n < 1)
		return {};

	// Wrap each of the input vector's elements in a vector, e.g.:
	//   [ 1, 2, 3, 4 ] ⇒ [ [1], [2], [3], [4] ]
	// This way we start off with the 1-fold cartesian product and the rest is generic.
	auto result = v
		| std::views::transform(
			[] (auto&& elem) {
				return std::vector{ elem };
			})
		| std::ranges::to<std::vector>()
	;

	// Repeatedly (compute result × v) until we've reached the desired arity.
	for(auto i = 0; i < n - 1; i++)
	{
		result = std::views::cartesian_product(result, v)
			| std::views::transform(
				[] (const auto& t) {
					auto v = std::get<0>(t);
					v.push_back(std::get<1>(t));
					return v;
				})
			| std::ranges::to<std::vector>()
		;
	}
	return result;
}


template<typename Container>
void
filter_by_mask(Container& c, const std::vector<bool>& mask)
{
	auto last_valid_p1 = std::min(c.size(), sizeof(mask) * 8);
	for(auto i = last_valid_p1; i-- > 0;)
	{
		if(!mask[i])
		{
			std::swap(c[i], c[last_valid_p1-- - 1]);
		}
	}
	c.resize(last_valid_p1);
}


std::generator<std::vector<int>>
gen_combinations(const std::vector<int>& numbers, int k)
{
	const auto n = numbers.size();
	auto mask = std::vector<bool>(n);
	std::fill(std::end(mask) - k, std::end(mask), true);

	do
	{
		auto combination = numbers;
		filter_by_mask(combination, mask);
		co_yield combination;
	}
	while(std::ranges::next_permutation(mask).found);
}


template<typename C>
std::generator<std::vector<typename C::value_type>>
gen_permutations(const C& c)
{
	auto permutation = std::vector<typename C::value_type>{ c };
	std::ranges::sort(permutation);
    do
    {
    	co_yield permutation;
    }
	while(std::ranges::next_permutation(permutation).found);
}


template<typename Clock = std::chrono::high_resolution_clock, typename F>
Clock::duration
measure_runtime(F&& f)
{
	const auto before = Clock::now();
	f();
	const auto after = Clock::now();
	return after - before;
}


template<typename Precision>
auto
duration_count(auto d)
{
	return std::chrono::duration_cast<Precision>(d).count();
}


auto
duration_us(auto d)
{
	return duration_count<std::chrono::microseconds>(d);
}


template<std::ranges::input_range R, typename T = std::ranges::range_value_t<R>>
T
average(R&& r)
{
	const auto sum = std::ranges::fold_left(r, T{}, std::plus{});
	return sum / std::ranges::ssize(r);
}


//
// Arithmetic expressions

enum class Operator
{
	Add,
	Subtract,
	Multiply,
	Divide,
};

constexpr auto Operators = std::array{
	Operator::Add,
	Operator::Subtract,
	Operator::Multiply,
	Operator::Divide,
};


class BinaryOp;
using Value = int;
using Expr = std::variant<BinaryOp, Value>;


class BinaryOp final
{
	std::unique_ptr<Expr>	left_;
	Operator				op_{};
	std::unique_ptr<Expr>	right_;

public:
	BinaryOp(Expr left, Operator op, Expr right)
		: left_{ std::make_unique<Expr>(std::move(left)) }
		, op_{ op }
		, right_{ std::make_unique<Expr>(std::move(right)) }
	{}

	const Expr& GetLeft() const { return *left_; }
	Operator GetOp() const { return op_; }
	const Expr& GetRight() const { return *right_; }
};


auto
format_as(Operator op)
{
	using enum Operator;
	switch(op)
	{
		case Add:		return "+";
		case Subtract:	return "-";
		case Multiply:	return "*";
		case Divide:	return "/";
		default:		return "<?>";
	}
}

std::string format_as(const Expr& expr);

std::string
format_as(const BinaryOp& binOp)
{
	return fmt::format("({} {} {})",
		binOp.GetLeft(),
		binOp.GetOp(),
		binOp.GetRight()
	);
}

std::string
format_as(const Expr& expr)
{
	return std::visit(
		[] (const auto& e) { return fmt::format("{}", e); },
		expr
	);
}


//
// Helper functions

Expr
make_expr(std::vector<int> numbers, std::vector<Operator> operators)
{
	if (operators.size() + 1 != numbers.size())
		throw std::runtime_error{ "arity mismatch" };

	auto r = Expr{ Value{ numbers.back() } };
	numbers.pop_back();
	while(!numbers.empty())
	{
		auto l = Value{ numbers.back() };
		numbers.pop_back();
		const auto op = operators.back();
		operators.pop_back();
		r = BinaryOp{ std::move(l), op, std::move(r) };
	}
	return r;
}


std::expected<int, std::string>
eval_expr(const Expr& expr)
{
	static constexpr auto visitor = Functor{
		[] (this auto const& self, const BinaryOp& binOp) -> std::expected<int, std::string> {
			// Note that the type of 'self' is not the type of the lambda's closure object but
			// the type of the 'Functor' object that derives from it. This is because the
			// deduced type is always the most derived, statically known, type.
			const auto leftResult = std::visit(self, binOp.GetLeft());
			if(!leftResult)
				return leftResult;
			const auto rightResult = std::visit(self, binOp.GetRight());
			if(!rightResult)
				return rightResult;
			switch(binOp.GetOp())
			{
				using enum Operator;
				case Add:
					return *leftResult + *rightResult;
				case Subtract:
					return *leftResult - *rightResult;
				case Multiply:
					return *leftResult * *rightResult;
				case Divide:
					if(*rightResult == 0)
						return std::unexpected{ "division by 0" };
					if(*leftResult % *rightResult != 0)
						return std::unexpected{ "non-integer division result" };
					return *leftResult / *rightResult;
				default:
					throw std::logic_error{ "bad operator" };
			}
		},
		[] (Value value) -> std::expected<int, std::string> {
			return value;
		},
	};

	return std::visit(visitor, expr);
}


void
solve(const std::vector<int>& inputNumbers, int inputResult, bool verbose, bool nonSolutions)
{
	// Create a vector of all combinations of (n - 1) numbers from the n input numbers.
	auto combNumbers = gen_combinations(inputNumbers, inputNumbers.size() - 1)
		| std::ranges::to<std::vector>();
	if(verbose)
		fmt::println("Combinations:\n{}\n", fmt::join(combNumbers, "\n"));

	// Create a vector of all permutations of the above (n-1)-number-sets.
	auto operands = combNumbers
		| std::views::transform(
			[] (const std::vector<int>& numberSubset) {
				return gen_permutations(numberSubset) | std::ranges::to<std::vector<std::vector<int>>>();
			})
		| std::views::join
		| std::ranges::to<std::vector>()
	;
	if(verbose)
		fmt::println("Operands ({}):\n{}\n", operands.size(), fmt::join(operands, "\n"));

	// Create a vector containing the set of all possible (n-2)-operator-sets.
	// These are the (n - 2) operators that go between the (n - 1) operands to form arithmetic
	// expressions.
	const auto operators = cartesian_self_product(Operators, inputNumbers.size() - 2);
	if(verbose)
		fmt::println("Operators ({}):\n{}\n", operators.size(), fmt::join(operators, "\n"));

	// Produce a list of valid equations, i.e. arithmetic expressions whose result matches the
	// input result.
	// We start off with the cartesian product of operands and operators ...
	auto solutions = std::views::cartesian_product(operands, operators)
		// ... then transform each resulting { operands, operators } tuple into an expression tree
		// that we evaluate ...
		| std::views::transform(
			[] (const auto& t) { // t is a tuple<const vector<int>&, const vector<Operator>&>
				auto expr = std::apply(make_expr, t);
				const auto result = eval_expr(expr);
				return std::tuple{
					std::move(expr),
					result,
				};
			})
		// ... then filter the resulting { Expr, int } tuple to only keep the ones whose result
		// matches the input result ...
		| std::views::filter(
			[inputResult, nonSolutions] (const auto& t) {
				if(nonSolutions)
					return true;
				const auto& evalResult = std::get<1>(t);
				return evalResult == inputResult;
			})
		// ... and finally turn them into nicely formatted strings.
		| std::views::transform(
			[inputResult, nonSolutions] (const auto& t) -> std::string {
				if(nonSolutions)
				{
					const auto& evalResult = std::get<1>(t);
					if(!evalResult)
						return fmt::format("{} = n/a ≠ {}", std::get<0>(t), inputResult);
					else if(*evalResult == inputResult)
						return fmt::format("{}{} = {}{}", ColorGreen, std::get<0>(t), *std::get<1>(t), ColorNone);
					else
						return fmt::format("{}{} = {} ≠ {}{}", ColorRed, std::get<0>(t), *std::get<1>(t), inputResult, ColorNone);
				}
				return fmt::format("{} = {}", std::get<0>(t), *std::get<1>(t));
			})
	;
	if(verbose)
		fmt::println("Solutions:");
	fmt::println("{}", fmt::join(solutions, "\n"));
}


}


int
main(int argc, char **argv)
{
	// Argument parsing
	auto inputNumbers = std::vector<int>{};
	auto inputResult = int{};
	auto verbose = false;
	auto nonSolutions = false;
	auto benchmark = false;
	auto iterations = 100;
	{
		auto app = CLI::App{ "Solve number flowers." };
		argv = app.ensure_utf8(argv);

		app.add_option("-n,--numbers", inputNumbers, "Input numbers (petals)")->required();
		app.add_option("-r,--result", inputResult, "Input result (stamen)")->required();
		app.add_flag("-v,--verbose", verbose, "Enable verbose output");
		app.add_flag("-s,--non-solutions", nonSolutions, "Output non-solutions as well");
		app.add_flag("-b,--benchmark", benchmark, "Enable benchmarking (consider > /dev/null)");
		app.add_option("-i,--iterations", iterations, "Number of runs when benchmarking");

		CLI11_PARSE(app, argc, argv);
	}

	if(verbose)
	{
		fmt::println("Numbers (petals): {}", inputNumbers);
		fmt::println("Result (stamen):  {}\n", inputResult);
	}

	if(!benchmark)
	{
		solve(inputNumbers, inputResult, verbose, nonSolutions);
	}
	else
	{
		using Clock = std::chrono::high_resolution_clock;
		auto durations = std::vector<Clock::duration>(iterations);
		for(auto& duration : durations)
		{
			duration = measure_runtime<Clock>([&] {
				solve(inputNumbers, inputResult, verbose, nonSolutions);
			});
		}
		fmt::println(stderr, "Duration: {} μs (average of {} runs)",
			duration_us(average(durations)),
			durations.size()
		);
#if ENABLE_ALLOCATION_TRACKING
		fmt::println(stderr, "Allocations: {}", AllocStat);
#endif
	}

    return 0;
}
